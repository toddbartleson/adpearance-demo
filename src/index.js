const config = {
  aws: {
    iam: {
      accessKeyId: 'AKIA3HJRUQ5XUZTG2G6C',
      secretAccessKey: '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',
      region: 'us-west-2',
    },
    s3: {
      bucket: 'foureyes-dev-test',
      key: 'db-token.json',
    },
  },
  database: {
    host: 'foureyes-dev-test.cq3u2jycwtka.us-west-2.rds.amazonaws.com',
    user: 'applicant',
    password: null,
    database: 'foureyes',
  },
};

const AWS = require('aws-sdk');
const mysql = require('mysql2/promise');
const axios = require('axios');

AWS.config.update(config.aws.iam);
const s3 = new AWS.S3();

(async () => {
  // write your code here...


  /* The below exercises flow from 1 to 2 to 3. You can't do 2 without doing 1 first, and you can't do 3 without doing 1 then 2.
      If you get stuck, feel free to email with questions, but of course we'll take this into consideration when we evaluate you as an applicant.
      It should go without saying that we expect your code to be as clear and concise as possible, with comments where you see appropriate.
      The AWS/s3, mysql, and axios libraries defined above are all async/await (promise) compatible, and that method is preferred over a callback approach.
      Lastly, no other libraries other than those defined above should be used, and we expect your code to use ES6 standards where applicable.
  */

  //git clone https://toddbartleson@bitbucket.org/toddbartleson/adpearance-demo.git

  //iife for private scope, would do this as a module without this demo's constraints
  const AccountsReportGenerator = (function(){

    /*
    EXERCISE 1: Using Amazon's S3 JavaScript SDK (http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html)...
      - Get the json file in the bucket defined above in the config.aws.s3 object
      - This json file will contain an object, { "dbToken": "THE DB TOKEN" }
      - Note: The AWS and S3 connection has already been established above

      The purpose of this exercise is to test your ability to follow Amazon documentation.
    */
    function fetchDbToken() {
      return s3.getObject({
          Key: config.aws.s3.key,
          Bucket: config.aws.s3.bucket
        })
        .promise()
        .then(data => {
          return extractDbToken(data.Body);
        });
    }

    function extractDbToken(buffer) {
      let json = ab2str(buffer);

      //idk why but there's extra characters at the beginning of the json blob
      //after I convert the array buffer to a string
      //anyway, this seems like a safe way to clean the blob's text
      json = json.slice(json.indexOf('{'));

      return JSON.parse(json).dbToken;
    }

    // http://stackoverflow.com/a/11058858
    // I do NOT love this
    // I haven't really worked with ArrayBuffers before and I found this code snippet on several sites
    // but it's not returning me a clean json blob from the given buffer data
    function ab2str(buf) {
      return String.fromCharCode.apply(null, new Uint16Array(buf));
    }


    /*
    EXERCISE 2: Using the mysql2/promise library (https://www.npmjs.com/package/mysql2)...
      - Use THE DB TOKEN (password) obtained above to create a database connection
      - Note: All other required db credentials are defined above in the config.database object
      - With the established database connection, execute a query that obtains the following...
        - Get the account token from the 'Paid' account type with the most recent last modified date
        - The schema is defined as...
          - accounts (id, account_name, account_type, account_token_id, last_modified)
          - account_tokens (id, account_token)
          - Note: accounts.account_token_id is a foreign key to account_tokens.id
        - Read only please. Under no circumstance should you update or insert any records in the database.

      The purpose of this exercise is to test your ability to use a database library,
        to understand a schema, and to write a simple join query.
    */
    function fetchAccountToken(dbToken) {
      if (!dbToken) {
        throw 'fetchAccountToken: empty dbToken!';
      }

      const credentials = config.database;
      credentials.password = dbToken;

      let connection;

      //inline sql is not best practice..
      //but for this purpose, obviously there's no way around it
      const query =
  `select at.account_token
  from accounts a
    inner join account_tokens at on at.id = a.account_token_id
  where a.account_type = 'Paid'
  order by a.last_modified desc
  limit 1`;

      return mysql
        .createConnection(credentials)
        .then(conn => {
          connection = conn;
          return connection.query(query);
        })
        .then(data => {
          connection.end();

          const rows = data[0];
          return rows[0].account_token;
        });
    }

    /*
    EXERCISE 3: Using the axios library (https://www.npmjs.com/package/axios)...
      - Make a get request to https://backend.foureyes.io/foureyes-dev-test with a single get param...
        - accountToken (obtained above)
      - This request will return a large json object, defined as...
        {
          "accountsHours": [...],
          "accounts": [...],
          "leads": [...],
        }

        The purpose of this excercise is to evaluate the way you think. There are countless ways to program functions
          that do the above, but we are looking for the most concise and straight forward way to achieve the end goal.
    */
    function fetchAccountsData(accountToken) {
      if (!accountToken) {
        throw 'fetchAccountsData: empty accountToken!';
      }

      //https://backend.foureyes.io/foureyes-dev-test with a single get param...
      //  - accountToken (obtained above)

      //could be part of some config, and passed
      //as a parameter of this function
      const url = 'https://backend.foureyes.io/foureyes-dev-test';

      return axios
        .get(url, {
          params: { accountToken }
        })
        .then(response => {
          const accountsData = response.data;
          return accountsData;
        });
    }


    /*
    - Using the accountsHours array, write a function that returns an object defined as...
    {
      "latest_monday_open": [array of account ids that have the latest monday open time],
      "earliest_monday_close": [array of account ids that have the earliest monday close time],
      "latest_tuesday_open": [same as above but for tuesday],
      "earliest_tuesday_close": [same as above but for tuesday],
      "latest_wednesday_open": [same as above but for wednesday],
      "earliest_wednesday_close": [same as above but for wednesday],
      "latest_thursday_open": [same as above but for thursday],
      "earliest_thursday_close": [same as above but for thursday],
      "latest_friday_open": [same as above but for friday],
      "earliest_friday_close": [same as above but for friday],
      "latest_saturday_open": [same as above but for saturday],
      "earliest_saturday_close": [same as above but for saturday],
      "latest_sunday_open": [same as above but for sunday],
      "earliest_sunday_close": [same as above but for sunday],
    }
    - Note: For example, if two accounts (account id x and y) share the latest monday open time of 10am (10:00:00),
          the array would be [x,y]

      let accountTime = {
        account_id:305,
        friday_close:"19:00:00",
        friday_open:"09:00:00",
        monday_close:"19:00:00",
        monday_open:"09:00:00",
        saturday_close:null,
        saturday_open:null,
        sunday_close:null,
        sunday_open:null,
        thursday_close:"19:00:00",
        thursday_open:"09:00:00",
        tuesday_close:"19:00:00",
        tuesday_open:"09:00:00",
        wednesday_close:"19:00:00",
        wednesday_open:"09:00:00"
      };

    //*/

    const weekDays = 'monday,tuesday,wednesday,thursday,friday,saturday,sunday'.split(',');

    function collateMinAndMaxAccountOperationTimes ({accountsHours}) {
      const collated = initEmptyMinAndMaxAccountOperationTimes();
      accountsHours.forEach(collateAccountHours.bind(null, collated));
      return formatResults(collated);
    }

    function initEmptyMinAndMaxAccountOperationTimes() {
      const collated = {};
      weekDays
        .forEach(wd => {
          collated[wd] = {
            open: {
                best: 0,
                time: null, //keeping a copy of the min/max opening/closing time, seems relavant to the report even though it's not in the spec
                accountIds: []
            },
            close:  {
                best: 1000000000, //pick a large time so the first encountered opening time will be less, and thus assigned as the min opening time
                time: null,
                accountIds: []
            }
          };
        });

      return collated;
    }

    //my initial implementation didn't follow the spec,
    //I was finding the latest closing times
    //and earliest opening times,
    //turned out they were all at one "24" hour location...
    //but it's easy to spin that around by
    //altering the passed compare functions
    function collateAccountHours(collated, record) {
      weekDays
        .forEach(wd => {
          const weekDay = collated[wd];

          evaluateRecordField(weekDay, 'open', record, `${wd}_open`, (a, b) => a > b);
          evaluateRecordField(weekDay, 'close', record, `${wd}_close`, (a, b) => a < b);
        });
    }

    //i don't love this function name,
    //it's sort of a broad, abstract operation, kind of hard to name well
    //the idea is that we're going to narrowly inspect a current accountsHours' record field
    //and decide if that field represents a 'best' time observed for the given day and operation
    //and if so, record that time as the new best and start clean list of account ids,
    // -or-
    //if the field equals a best time, then append that slot's account ids list
    function evaluateRecordField(weekDay, operation, record, fieldName, compareFunc) {
      if (record[fieldName]) {
        const time = convertTime(record[fieldName]);
        if (time == weekDay[operation].best) {
          weekDay[operation].accountIds.push(record.account_id);
        }
        else if (compareFunc(time, weekDay[operation].best)) {
          weekDay[operation].best = time;
          weekDay[operation].time = record[fieldName];
          weekDay[operation].accountIds = [ record.account_id ];
        }
      }
    }

    //re-package the collated results to match the spec
    function formatResults(collated) {
      const results = {};

      for (weekDay in collated) {
        results[`latest_${weekDay}_open`] = collated[weekDay].open.accountIds;
        results[`earliest_${weekDay}_close`] = collated[weekDay].close.accountIds;
      }

      return results;
    }

    //this is admittly a little janky,
    //basically, I appreciate that converting and sorting dates is complex
    //I want to compare times as scalars for the bracketing task
    //but this demo data's time fields are uniform enough that I can just
    //turn them into ints, as opposed to actually making Date objects
    const convertTime = (timeString) => parseInt(timeString.replace(':', '').replace(':', ''));


  /*
    - Using the leads array, write a function that returns an object defined as...
    {
      x: {
        Calls: {
          Paid: count of paid call leads for account id x,
          Organic: count of organic call leads for account id x,
          Social: count of social call leads for account id x,
          Direct: count of direct call leads for account id x,
          Referral: count of referral call leads for account id x,
        },
        Forms: {
          same as above but for forms
        },
        Chats: {
          same as above but for chats
        },
      },
      y: {
        same as avove but for account id y
      },
      etc
    }
    - Note: x and y are the account ids. Do this for all accounts with leads.
      Remove any account from the object that has no leads.

    lead: {
      lead account_id:198
      lead_timestamp:"2015-09-30T21:54:03.000Z"
      lead_type:"Call"
      outcome:Object {valid: false, sold: false}
      source:Object {host: "www.google.com", type: "Paid"}
    }
  //*/

    const leadTypes = 'Call,Form,Chat'.split(',');
    const leadSourceTypes = 'Paid,Organic,Social,Direct,Referral'.split(',');

    function sumLeadCounts({leads}) {
      const leadCounts = {};
      leads.forEach(aggregateLeads.bind(null, leadCounts));
      return leadCounts;
    }

    function aggregateLeads(leadCounts, current) {
      let accountHandle = leadCounts[current.account_id];
      if (!accountHandle) {
        leadCounts[current.account_id] = accountHandle = initEmptyLeadSums();
      }

      //the spec says use the plural forms of the lead_type...
      accountHandle[`${current.lead_type}s`][current.source.type]++;
    }

    function initEmptyLeadSums() {
      const sums = {};
      leadTypes
        .forEach(lt => {
          sums[`${lt}s`] = {};
          leadSourceTypes.forEach(st => sums[`${lt}s`][st] = 0);
        });

      return sums;
    }


    class AccountsReportGenerator {
      generateReport() {
        return fetchDbToken()
          .then(fetchAccountToken)
          .then(fetchAccountsData)
          .then(accountsData => {
            return Promise.all([
              collateMinAndMaxAccountOperationTimes(accountsData),
              sumLeadCounts(accountsData)
            ]);
          })
          .catch(err => console.log(`error: ${JSON.stringify(err)}`));
      }
    }

    AccountsReportGenerator.leadTypes = leadTypes;

    //We have not specfically addressed testing,
    //but to that end, one might expose private module functions
    //for unit testing like this,
    //or even wrap these assignments in a if (exposeForTesting)
    //sort of conditional, if you wanted to not expose those
    //names in production code
    //the 2 functions list below are the best candidates for
    //unit testing in this module
    //the 3 proceeding operations aren't as critical
    //for testing as they are all-or-nothing catastrophic failure type operations
    //the bracketing task though is complex enough to warrant a set of tests
    //I would grab small chunks of the data set and compute the
    //min and max closing and opening times by hand,
    //then execise the functions with those inputs and expect my computed results.
    AccountsReportGenerator.collateMinAndMaxAccountOperationTimes = collateMinAndMaxAccountOperationTimes;
    AccountsReportGenerator.sumLeadCounts = sumLeadCounts;

    return AccountsReportGenerator;
  })();

  (new AccountsReportGenerator())
    .generateReport()
    .then(reportObjects => {
      //so, these reports objects are the spec'd results of exercises 3 a & b
      //I've provided a couple rendering functions because
      //if you're going to run 'npm start' then you presumably
      //want some kind of output that doesn't involve stepping into the
      //code to the verify the results
      //but if you do step in, this is good place for a break point
      const minAndMaxAccountOperationTimes = reportObjects[0];
      const leadCounts = reportObjects[1];

      renderMinAndMaxAccountOperationTimes(minAndMaxAccountOperationTimes);
      renderLeadCounts(leadCounts);

      console.log('Done');
    });

  //report object rendering functions
  function renderMinAndMaxAccountOperationTimes(minAndMaxAccountOperationTimes) {
    console.log('-------------------------------------------------------------------------');
    console.log('---minAndMaxAccountOperationTimes----------------------------------------');
    for (const key in minAndMaxAccountOperationTimes) {
      console.log(`${key} by accounts [${minAndMaxAccountOperationTimes[key]}]`);
    }
  }

  function renderLeadCounts(leadCounts) {
    console.log('-------------------------------------------------------------------------');
    console.log('---leadCounts------------------------------------------------------------');
    for (const key in leadCounts) {
      console.log(`For account id: ${key}`);
      AccountsReportGenerator.leadTypes.forEach(lt => {
        lt = `${lt}s`;
        console.log(`   ${lt}: ${JSON.stringify(leadCounts[key][lt])}`);
      });
    }
  }

})();
